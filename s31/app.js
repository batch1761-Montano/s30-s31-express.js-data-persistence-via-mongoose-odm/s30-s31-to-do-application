//Mini Activity:
/*
	Setup a basic Express JS server.

*/
//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//[SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

	const port = 4000;

//[SECTION] Database Setup
	mongoose.connect('mongodb+srv://jmv014:admin123@cluster0.uciuv.mongodb.net/toDo176?retryWrites=true&w=majority',{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});

	let db = mongoose.connection;
	
	db.on('error',console.error.bind(console,"Connection Error"));
	
	db.once('open',()=> console.log("Connected to MongoDB"));

	//Add task route
//[SECTION] Routing Systems
	app.use("/tasks", taskRoute);
	
//[SECTION] Entry point response
app.listen(port, (req, res) => console.log(`Server running at port ${port}`));