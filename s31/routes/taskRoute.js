//Contains all the endpoints for our application
//We separate the routes such that "app.js" only contains information on the server

const express = require("express")

//Create a Router instance that functions as a middleware and routing system
//Allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskControllers")

//[Routes]
//The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed

//Route for getting all the tasks
router.get("/", (req, res) => {

	//Invoke the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
	//"result"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//Route for creating a task
router.post("/", (req, res) => {

	//The "createTask" function needs the data from the request body, so we need to supply it to the function
	//If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property 
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))

})


//Route for Deleting a task
	//call out the routing component to register a brand new endpoint.
	//when integrating a path variable within the URI, you are also changing the behavior of the path from STATIC to DYNAMIC.
	//2 Types of End points
		//Static Route
			// -> unchanging, fixed, constant, dynamic
		//Dynamic Route	
			// -> interchangable or NOT FIXED.
router.delete('/:task', (req, res) => {
	//identify the task to be executed within this endpoint
	//call out the proper function for this route and identify the source/provided of the function.
	console.log(req.params.task);
	//place the value of the path variable inside its own container.
	let taskId = req.params.task;
	//res.send('Hello from delete');
	//retrieve the identity of the task by inserting the ObjectId of the resource.
	//make sure to pass down the identity of the task using the proper reference (objectID), however this time we are going to include the information as the path variable.
	//Path variable -> this will allow us to insert data within the scope of the URL.
		//what is variable? -> container, storage information
	//When is a path variable useful? 
		// -> when inserting only a single piece of information.
		// -> this is also useful when passing down information to Rest API method that does not include a BODY section like 'GET'.	
	taskController.deleteTask(taskId).then(resultOfDelete => res.send(resultOfDelete));
});	

//Route for Updating task status (pending -> Complete)
	//Lets create a dynamic endpoint for this brand new route.

router.put("/:task", (req, res) => {
	//check of you are able to acquire the values inside the path variables.

	//console.log(req.params.task);//check if the value inside the parameters of the route is properly transmitted to the server.
	let idNiTask = req.params.task; //this is declared to simplify the means of calling out the value of the path variable key.

	//identify the business logic behind this task inside the controller module.
	//call the intended controller to execute the process.
	//after invoking the controller method handle the outcome.
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
});

//Route for Updating task status (Completed -> Pending)
	//This will be a counter procedure for the previous task.
router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	//console.log(id)

	//declare the business logic aspect of this brand new task in our app.
	//invoke the task you want to execute this route.
	taskController.taskPending(id).then(outcome =>{
		res.send(outcome);
	})
});	

//Use "module.exports" to export the router object to use in the "app.js"
module.exports = router;
