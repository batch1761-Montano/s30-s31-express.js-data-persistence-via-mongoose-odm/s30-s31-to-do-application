//Controllers contains the functions and business logic of our Express JS Application

//Uses the "require" directive to allow access to the "Task" model which allows us to access mongoose methods to perform CRUD functions
//Allows us to use the contents of the "task.js" file in the "models" folder
const Task = require("../models/task");

//[SECTION] - CONTROLLERS 

//[SECTION] - Create
	//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
	module.exports.createTask = (requestBody) => {


		//Creates a task object based on the Mongoose model "Task"
		let newTask = new Task({

			//Sets the "name" property with the value received from the client/Postman
			name: requestBody.name
		})


		// Saves the newly created "newTask" object in the MongoDB database
		// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
		// The "then" method will accept the following 2 arguments:
			// The first parameter will store the result returned by the Mongoose "save" method
			// The second parameter will store the "error" object
		// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
		// Ex.
			// newUser.save((saveErr, savedTask) => {})
		return newTask.save().then((task, error) => {

			// If an error is encountered returns a "false" boolean back to the client/Postman
			if(error) {
				return false
			// Save successful, returns the new task object back to the client/Postman
			} else {
				return task
			}
		})


	}
//[SECTION] - Retrieve
	module.exports.getAllTasks = () => {

		//The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function the "/tasks" routes is accessed

		return Task.find({}).then(result => {

			//The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
			return result
		})
	};

//[SECTION] - Update
	//1. Change status of Task. pending -> 'completed'.
	//we will reference the document using it's ID field.
	module.exports.taskCompleted = (taskId) => {
		//Query/search for the desired task to update.
		//The "findById" mongoose method will look for a resource (task) which matches the ID from the URL of the request.
		//upon performing this method inside our collection, a new promise will be instantiated, so we need to be able to handle the possbile outcome of theat promise.
		return Task.findById(taskId).then((found, error) => {
			//describe how were going to use handle the outcome of the promise using a selection control structure.
			//error -> rejected state of the promise
			//process the document found from the collection and change the status from 'pending' => 'completed'
			if (found) {
				console.log(found); //document found from the db should be displayed in the terminal.
				//Modified the status of the returned document to 'completed'
				found.status = 'Completed';
				//Save the new changes inside our database.
				//upon saving the new changes for this document a 2nd promise will be instantiated.
				//we will chain a thenable expression upon performing a save() method into our returned document.
				return found.save().then((updatedTask, saveErr)=>{
					//catch the state of the promise to identify a specific response.
					if (updatedTask) {
						return 'Task has been successfully updated'
					} else {
						//return the actual error
						return 'Task failed to Update';
					}
				});
			} else {
				//call out the parameter that describes the result of the query when successful.
				return 'Error! No Document Found';
			}
				

		})
	};

	//2. Change the status of task (Completed -> Pending)
	module.exports.taskPending = (userInput) => {
		//expose this new component
		//search the database for the user Input.
		//findById mongoose method -> will run a search query inside our database using the id field as it's reference.
		//since performing this method will chain a then expression to handle the possilbe states of the promise.
		//assign and invoke this new controller task to its own separate route.
		return Task.findById(userInput).then((result, err) =>{
			//handle and catch the state of the promise.
			if (result) {
				//process the result of the query and extract the property to modify it's value.
				result.status = 'Pending'

				//save the new changes in the document
				return result.save().then((taskUpdated, error) =>{
					//create a control structure to identify the proper response if the updates was successfully executed.
					if (taskUpdated) {
						return `Task ${taskUpdated.name} was updated to Pending`
					} else {
						return 'Error when saving task updates';
					}
				});
			} else {	
				return	`Something Went Wrong`;
			}
		});
	};
//[SECTION] - Destroy

	// 1. Remove an existing resource inside the TASK collection.
		//expose the data across other modules other app so that it will become reusable.
		// would we need an input from the user? => which resource you want to target.

	module.exports.deleteTask = (taskId) => {
		//how will the data will be processed in order to execute the task.
		//select which mongoose method will be used in order to acquire the desired end goal.
		//Mongoose -> it provides an interface and methods to be able to manipulate the resoures found inside the mongoDB atlas.
		//in order to identify the location in which the function will be executed append the model name.

		//findByIdAndRemove => this is a mongoose method which targets a document using it's id field and removes the targeted document from the collection.
		//upon executing this method within our collection a new promise will be instantiated upon waiting for the task to be executed, however we need to be able to handle the outcome of the promise whatever state in may fall on.
		//Promises in JS
			//=> Pending (waiting to be executed)
			//=> Fulfilled (successfully executed)
			//=> Rejected (unfullfilled promise)
		//to be able to handle the possible states upon executing the task.
		//we are going to insert a 'thenable' expression to determine HOW we will respond depending on the result of the promise.	
		//identify the 2 possible state of the promise using a then() expression.
		return Task.findByIdAndRemove(taskId).then((fulfilled,rejected) => {
			//identify how you will act according to the outcome.
			if (fulfilled) {
				return 'The Task has been successfully removed';
			} else {
				return 'Failed to remove Task'
			}
			//assign a new endpoint for this route.

		});
	};